package zadanie2;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Message {
    private int id;
    private String messanger;

    public Message(int id, String messanger) {
        this.id = id;
        this.messanger = messanger;
    }

}

