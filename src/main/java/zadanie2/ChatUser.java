package zadanie2;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

@Getter
@Setter
public class ChatUser implements Observer {
    private int id;
    private String nick;
    private List<String> messages;
    private boolean isAdmin = false;

    public ChatUser(int id, String nick) {
        this.id = id;
        this.nick = nick;
    }

    @Override
    public void update(Observable o, Object arg) {

    }

    @Override
    public String toString() {
        return "ChatUser{" +
                "id=" + id +
                ", nick='" + nick + '\'' +
                ", messages=" + messages +
                ", isAdmin=" + isAdmin +
                '}';
    }
}
