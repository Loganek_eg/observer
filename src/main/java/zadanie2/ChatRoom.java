package zadanie2;

import java.util.*;

public class ChatRoom extends Observable {
    private Map<Integer, ChatUser> loggedUsers = new HashMap<>();
    private String roomName;
    private int id = 0;
    private List<String> admins = new ArrayList<>();

    public ChatRoom(String givenRoomName) {
        this.roomName = givenRoomName;

        // add two nicks that are able to be administrators
        this.admins.add("Admin");
        this.admins.add("Administrator");
        this.admins.add("Tomek");
    }

    public void userLogin(String nick) {
        for (ChatUser user : loggedUsers.values()) {
            if (user.getNick().equals(nick)) {
                System.out.println(nick + "taki użytkownik już istnieje na kanale");
                return;
            }
        }

        ChatUser newChatUser = new ChatUser(id++, nick);
        loggedUsers.put(id, newChatUser);
        if (admins.contains(nick)) {
            newChatUser.setAdmin(true);
        }
        addObserver(newChatUser);

    }

    public void sendMessage(int user, String message) {

    }

    public void notifyAboutMessage(String myMessage) {
        setChanged();
        notifyObservers(myMessage);
    }

    public void kickUser(int id_kickowanego, int id_kickujacego) {
        if (!loggedUsers.containsKey(id_kickowanego) || !loggedUsers.containsKey(id_kickujacego)) {
            System.out.println("Jeden z użytkowników o takim id nie istnieje");
            return;
        }
        if (loggedUsers.get(id_kickujacego).isAdmin()) {
            loggedUsers.remove(id_kickowanego);
            System.out.println("Admin kicknął użytkownika o id " + id_kickowanego);
        } else {
            System.out.println("You have no privileges to kick users");
        }
    }

    public void dajListeUserow() {
        System.out.println(loggedUsers);
    }

    @Override
    public String toString() {
        return "ChatRoom{" +
                "loggedUsers=" + loggedUsers +
                ", roomName='" + roomName + '\'' +
                ", id=" + id +
                ", admins=" + admins +
                '}';
    }
}
