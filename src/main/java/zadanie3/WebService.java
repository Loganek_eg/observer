package zadanie3;

import java.util.Observable;

public class WebService extends Observable {
    MarketingDepartmanet marketing = new MarketingDepartmanet();
    ServiceDepartment serviceDepartament = new ServiceDepartment();
    FinanceDepartment financeDepartament = new FinanceDepartment();

    public WebService() {
        super.addObserver(serviceDepartament);
        super.addObserver(financeDepartament);
        super.addObserver(marketing);
    }

    public void processRequest(Request.RequestType requestType, String content, String name) {
        Request r = new Request(requestType, content, name);
        setChanged();
        notifyObservers(r);
    }
}

