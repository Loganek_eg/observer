package zadanie3;


import java.io.IOException;

import java.util.Observable;
import java.util.Observer;

public class MarketingDepartmanet implements Observer {

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Request && ((Request) arg).isOrder()) {
            try {
                Order order = new Order(((Request) arg).getContent(), ((Request) arg).getUserName());
                Database.INSTANCE.addRecord(order, DatabaseName.DB_ORDER);
                System.out.println("Request " + order.getContent() + " form user " + order.getName() + " is being handled by Marketing Department");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
