package zadanie3;

import lombok.Getter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

@Getter
public enum DatabaseName {
    DB_SERVICE("C:\\Users\\Logan\\Projekty\\Observer\\src\\main\\java\\zadanie3\\users.txt"),
    DB_ORDER("C:\\Users\\Logan\\Projekty\\Observer\\src\\main\\java\\zadanie3\\orders.txt"),
    DB_REQUESTS("C:\\Users\\Logan\\Projekty\\Observer\\src\\main\\java\\zadanie3\\requests.txt");

    private File file;
    private PrintWriter pw;

    DatabaseName(String path) {
        this.file = new File(path);
        try {
            this.pw = new PrintWriter(new FileWriter(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
