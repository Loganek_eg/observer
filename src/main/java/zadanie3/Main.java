package zadanie3;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        WebService webService = new WebService();
        webService.processRequest(Request.RequestType.ORDER,"Chce nowego laptopa", "Tomek");
        webService.processRequest(Request.RequestType.INFO,"Mam starego laptopa", "Tomek");
        webService.processRequest(Request.RequestType.SERVICE,"Naprawcie mi laptopa", "Tomek");
        webService.processRequest(Request.RequestType.COMPLAINT,"Mój laptop sie zepsuł", "Tomek");


    }
}
