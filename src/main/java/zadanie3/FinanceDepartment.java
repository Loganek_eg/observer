package zadanie3;


import lombok.Getter;


import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

public class FinanceDepartment implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Request) {
            try {
                incrementStatistics(((Request) arg).getRequestType());
                Database.INSTANCE.addRecord(arg, DatabaseName.DB_REQUESTS);
                System.out.println("Request" + ((Request) arg).getContent() + "from user" +
                        ((Request) arg).getUserName() + " is being recorded by FInance Departament");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    public void printStatistic() {
        System.out.println(FinanceStatisctics.INSTANCE.info + " info request recived");
        System.out.println(FinanceStatisctics.INSTANCE.service + " service request recived");
        System.out.println(FinanceStatisctics.INSTANCE.complaint + " complaint request recived");
        System.out.println(FinanceStatisctics.INSTANCE.order + " order request recived");
    }

    private int incrementStatistics(Request.RequestType requestType) {
        switch (requestType) {
            case INFO: {
                return FinanceStatisctics.INSTANCE.info++;
            }
            case ORDER: {
                return FinanceStatisctics.INSTANCE.order++;
            }
            case SERVICE: {
                return FinanceStatisctics.INSTANCE.service++;
            }
            case COMPLAINT: {
                return FinanceStatisctics.INSTANCE.complaint++;
            }
            default:
                throw new IllegalArgumentException("Wrong Type");
        }
    }

    @Getter
    private enum FinanceStatisctics {
        INSTANCE;
        private int info = 0;
        private int service = 0;
        private int order = 0;
        private int complaint = 0;
    }


}
