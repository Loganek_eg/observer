package zadanie3;

import lombok.Getter;

@Getter
public class Order {
    private String content;
    private String name;


    public Order(String content, String userName) {
        this.content = content;
        this.name = userName;
    }

    @Override
    public String toString() {
        return "{" + name + " " + content + '}';
    }
}
