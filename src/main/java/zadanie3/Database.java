package zadanie3;

import lombok.Getter;
import lombok.Setter;

import java.io.IOException;

@Getter
@Setter
public class Database {
    public static final Database INSTANCE = new Database();

    private Database() {

    }


    public void addRecord(Object object, DatabaseName databaseName) throws IOException {
        switch (databaseName) {
            case DB_REQUESTS: {
                DatabaseName.DB_REQUESTS.getPw().println(object);
                DatabaseName.DB_REQUESTS.getPw().flush();
                break;
            }
            case DB_ORDER: {
                DatabaseName.DB_ORDER.getPw().println(object);
                DatabaseName.DB_ORDER.getPw().flush();
                break;
            }
            case DB_SERVICE: {
                DatabaseName.DB_SERVICE.getPw().println(object);
                DatabaseName.DB_SERVICE.getPw().flush();
                break;
            }
        }
    }

}
